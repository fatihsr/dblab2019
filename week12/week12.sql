call selectAllCustomers();

call getCustomersByCity("Barcelona");

set @max_salary = 0; #In fact, we don't need to set a variable since OUT already is null initially
call highestSalary(@max_salary);
select @max_salary;

set @mcount = 0; #IN parameter doesn't have initial value, also in INOUT. Thus, we need initial value
call countGender(@m_count, 'M');

set @f_count = 0;
call countGender(@f_count, 'F');

select @m_count, @f_count;