CREATE DEFINER=`root`@`localhost` PROCEDURE `countGender`(INOUT cnt integer, IN g char(1))
BEGIN
	select count(Gender) into cnt from Employees where Gender = g;
END