CREATE DEFINER=`root`@`localhost` PROCEDURE `highestSalary`(OUT highest_salary float)
BEGIN
	select max(Salary) into highest_salary from Employees;
END